﻿using System;
using System.Collections.Generic;
using TFGUtils;

namespace App
{
    [Serializable]

    public class AppSettings
    {
        public List<TrafficLightData> TrafficSetup;

        public AppSettings(int segmentNumber) 
        {
            TrafficSetup = new List<TrafficLightData>(segmentNumber);
            for (int i = 0; i < TrafficSetup.Capacity; i++) 
            {
                TrafficSetup.Add(new TrafficLightData());
            }
        }

        /// <summary>
        /// Sanitizes the JSON settings:
        /// If there is an invalid value in the TrafficLightSequence that Traffic Light will be purged from the list.
        /// Green and Red timers are clamped to the min a max acceptable values.
        /// </summary>
        /// <returns></returns>
        public void SanitizeData()
        {
            string[] small = {"s", "small", "short,", "p", "pequeño", "pequeña", "corto", "corta"};
            string[] medium = {"m", "medium", "mediano", "mediana"};
            string[] large = {"l", "large", "big", "g", "grande", "amplio", "amplia"};
            for (int i = 0; i < TrafficSetup.Count; i++)
            {
                if (TrafficSetup[i].SegmentLength.EqualsAny(small))
                {
                    TrafficSetup[i].SegmentLength = "S";
                }else if (TrafficSetup[i].SegmentLength.EqualsAny(medium))
                {
                    TrafficSetup[i].SegmentLength = "M";
                }else if (TrafficSetup[i].SegmentLength.EqualsAny(large))
                {
                    TrafficSetup[i].SegmentLength = "L";
                }
                else
                {
                    TrafficSetup.RemoveAt(i);
                    i--;
                    continue; //if one entry is purged, jump to the next iteration with the new proper index 
                }
                // ToGreenTimer minimum value 
                if (TrafficSetup[i].ToGreenTimer <= 0.5f)
                {
                    TrafficSetup[i].ToGreenTimer = 0.5f;
                }
                // ToRedTimer minimum and maximum accepted values
                if (TrafficSetup[i].ToRedTimer <= 0.0f)
                {
                    if (i == TrafficSetup.Count - 1)
                    {
                        TrafficSetup[i].ToRedTimer = 1.0f; // If last traffic light, HAS TO turn red
                    }
                    else
                    {
                        TrafficSetup[i].ToRedTimer = 0.0f; // zero value will be treated as never turns red later
                        if (TrafficSetup[i].HasAmber == true)     //if has amber, max time to change green again is the same as for changing to red
                        {
                            if (TrafficSetup[i + 1].ToGreenTimer >= 2.6f)
                            {
                                TrafficSetup[i + 1].ToGreenTimer = 2.6f;
                            }
                        }
                    }
                }
                else if (TrafficSetup[i].ToRedTimer > 2.6f)
                {
                    TrafficSetup[i].ToRedTimer = 2.6f;
                }
            }
        }
    }
}