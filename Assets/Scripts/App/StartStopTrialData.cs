using System;
using System.Collections.Generic;

namespace App
{
    [Serializable]
    public class StartStopTrialData
    {
        public MeasureData[] StartStopResults;
    }
}