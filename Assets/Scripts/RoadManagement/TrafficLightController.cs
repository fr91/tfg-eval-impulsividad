﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using App;
using Driving;
using UnityEngine;
using TFGUtils;

namespace RoadManagement
{
    public enum LightsColor
    {
        Off, Red, Amber, Green
    }
    
    [SuppressMessage("ReSharper", "BitwiseOperatorOnEnumWithoutFlags")]
    public class TrafficLightController : MonoBehaviour
    {
        // Global References
        private TrafficLightController[] trafficControllers;    // A shorcut name for "AppControl.Global.TrafficArray" 

        // References to the player car
        // TO-DO decide what to do with this:
        /*
        private GameObject player;
        private CarUserController userController;
        */

        // Traffic Light vars to change the renderer color
        public LightsColor TrafficState;
        public Material RedLight;
        public Material AmberLight;
        public Material GreenLight;
        public Material OffLight;
        public GameObject GreenGlassRow, AmberGlassRow, RedGlassRow;
        private Renderer redRenderer, amberRenderer, greenRenderer;
        public BoxCollider StopLineCollider;        // The collider from is children that is the stop line one
        
        // Timing
        //// Timers
        public CountdownTimer RedTimer;
        public CountdownTimer GreenTimer;
        //// Time to turn lights in seconds 
        public float SecondsToGreen;
        public float SecondsToRed;
        //// Chronometer for measuring
        public float GreenTimestamp = -1f;

        // Trial control vars
        public int MyIndex;
        public bool HasAmber;
        public bool IsLast = false;
        public bool ReadyToPlayerTimestamp = false;
        
        // C# Event to be executed when a segment is completed 
        public delegate void SegmentCompletion();
        public event SegmentCompletion OnCompletion;

        public delegate void ChangeLight(int nextIndex);
        public event ChangeLight OnTurnRed;
        
        
        private void Awake()
        {
            redRenderer = RedGlassRow.GetComponent<Renderer>();
            amberRenderer = AmberGlassRow.GetComponent<Renderer>();
            greenRenderer = GreenGlassRow.GetComponent<Renderer>();

            redRenderer.material = OffLight;
            amberRenderer.material = OffLight;
            greenRenderer.material = OffLight;
            /*
            player = GameObject.FindWithTag("Player");
            if (player != null)
            {
                userController = player.GetComponent<CarUserController>();
            }
            */
        }

        private void Start()
        {
            trafficControllers = AppControl.Global.TrafficArray;
        }

        private void FixedUpdate()
        {
           CheckTimers(Time.fixedDeltaTime);
        }
        
        
        private void OnCarStopped(float stoppedSpot)
        {
            if (((HasAmber && SecondsToRed>0.0f && TrafficState == LightsColor.Amber) || TrafficState == LightsColor.Red) && !IsLast)
            {
                GreenTimer?.ForceUnbind();
                GreenTimer = new CountdownTimer(trafficControllers[MyIndex + 1].SecondsToGreen, TurnNextGreenCountdown);
                GreenTimer.StartTimer();
                
                ReadyToPlayerTimestamp = true;
            }
        }

        public void TurnGreen()
        {
            redRenderer.material = OffLight;
            amberRenderer.material = OffLight;
            greenRenderer.material = GreenLight;
            TrafficState = LightsColor.Green;
        }

        public void TurnAmber()
        {
            redRenderer.material = OffLight;
            amberRenderer.material = AmberLight;
            greenRenderer.material = OffLight;
            TrafficState = LightsColor.Amber;
        }

        public void TurnRed()
        {
            redRenderer.material = RedLight;
            amberRenderer.material = OffLight;
            greenRenderer.material = OffLight;
            TrafficState = LightsColor.Red;
        }
        
        public void ReceiveOuterTrigger(Collider outer, Collider car)
        {
            CarUserController.OnStop += OnCarStopped;
            if (MyIndex == 0)
            {
                TurnRed();
                OnTurnRed?.Invoke(MyIndex);
                GreenTimer = new CountdownTimer(SecondsToGreen, TurnGreenCountdown);
                GreenTimer.StartTimer();
                ReadyToPlayerTimestamp = true;
            }
        }

        public void ReceiveMiddleTrigger(Collider outer, Collider car)
        {
            if(HasAmber)
                TurnAmber();
            if (SecondsToRed > 0.0f)
            {
                RedTimer = new CountdownTimer(SecondsToRed, TurnRedCountdown);
                RedTimer.StartTimer();
            }
            else if (!IsLast)
            {
                    GreenTimer?.ForceUnbind();
                    GreenTimer = new CountdownTimer(trafficControllers[MyIndex + 1].SecondsToGreen, SkipToNextGreenCountdown);
                    GreenTimer.StartTimer();   
            }
        }

        public void ReceiveStopLineTriggerEnter(Collider stopLine, Collider car)
        {
            CarUserController playerController = car.GetComponent<CarUserController>();
            if (TrafficState == LightsColor.Red)
            {
                playerController.StopImmediately();
                //print("LIMITE STOP PASADO: " + stopLine.transform.position.z);
            }
        }
        
        public void ReceiveStopLineTriggerExit(Collider stopLine, Collider car)
        {
            CarUserController.OnStop -= OnCarStopped;
            OnCompletion?.Invoke();
        }

        private void TurnRedCountdown()
        {
            TurnRed();
            if (!IsLast)
            {
                trafficControllers[MyIndex+1].TurnRed();
                OnTurnRed?.Invoke(MyIndex + 1);
            }
        }

        private void TurnGreenCountdown()
        {
            TurnGreen();
            GreenTimestamp = Time.time;
        }

        private void TurnNextGreenCountdown()
        {
            TurnGreen();
            trafficControllers[MyIndex+1].TurnGreen();
            trafficControllers[MyIndex+1].GreenTimestamp = Time.time;
        }
        
        private void SkipToNextGreenCountdown()
        {
            TurnGreen();
            trafficControllers[MyIndex+1].TurnGreen();
        }
        
        private void CheckTimers(float unityTimeElapsed)
        {
            GreenTimer?.TickCheck(unityTimeElapsed);
            RedTimer?.TickCheck(unityTimeElapsed);
        }

        public float GetStopColliderPosition()
        {
            return transform.TransformPoint(-StopLineCollider.bounds.extents).z;
        }
    }
}
