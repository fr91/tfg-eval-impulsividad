using UnityEngine;

namespace RoadManagement
{
    public class DetectRoadBoundaries : MonoBehaviour
    {
        public bool showRay = false;
        private Vector3 distance;

        // Use this for initialization
        void Start()
        {
            distance = Vector3.zero;
        }

        // Update is called every frame, if the MonoBehaviour is enabled
        private void Update()
        {
            if (showRay)
            {
                Debug.DrawRay(transform.position, transform.right * 15, Color.magenta);
            }
        }



        // Update is called once per frame
        void FixedUpdate()
        {
            RaycastHit hit;
            Ray horizontalRay = new Ray(transform.position, transform.right);
            Physics.Raycast(horizontalRay, out hit);
            distance = hit.point - transform.position;
            Debug.Log("Distance to fence: " + distance.magnitude + " metres");
        }
    }
}
