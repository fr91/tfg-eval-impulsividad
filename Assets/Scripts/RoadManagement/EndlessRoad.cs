﻿
using System;
using EasyRoads3Dv3;
using UnityEngine;
using System.Collections.Generic;
using App;
using UnityEditor;

namespace RoadManagement
{
    public class EndlessRoad : MonoBehaviour
    {
        private static ERRoadNetwork roadNetwork;
        
        public static int TerrainPoolSize = 0; // i.e.: Number of Sub-Terrains in the scene
        private static float terrainLength = 0.0f; // Terrain length in Z-axis

        // Pool of Traffic Lights
        public Transform TrafficLightParent;
        public static GameObject[] TrafficGameObjects;
        
        // Traffic Lights Prefabs
        public GameObject SmallTrafficLight;    // TL: Traffic Light
        public GameObject MediumTrafficLight;
        public GameObject LargeTrafficLight;
        private const float SmallSegment = 100f;
        private const float MediumSegment = 150f;
        private const float LargeSegment = 200f;

        // ERRoad's pool
        private static Queue<ERRoad> roadPool = new Queue<ERRoad>();
        private static int switcherIndex;

        // Awake is called when the script instance is being loaded.
        private void Awake()
        {
            GetTerrainInfo();
            InitRoadPool();
            InitTrafficArrays();
        }

        private void Start()
        {
        }

        private void GetTerrainInfo()
        {
            //TerrainPool = new Queue<Terrain>();
            GameObject[] goTerrains = GameObject.FindGameObjectsWithTag("TerrainPool");
            Terrain[] arrayOfTerrains = new Terrain[goTerrains.Length];
            for (int i = 0; i < goTerrains.Length; i++)
            {
                GameObject go = goTerrains[i];
                arrayOfTerrains[i] = go.GetComponent<Terrain>();
            }

            //Terrain[] arrayOfTerrains = FindObjectsOfType<Terrain>();
            terrainLength = arrayOfTerrains[0].terrainData.size.z;
            TerrainPoolSize = arrayOfTerrains.Length;
        }

        private void InitRoadPool()
        {
            roadNetwork = new ERRoadNetwork();
            foreach (ERRoad road in roadNetwork.GetRoads())
            {
                roadPool.Enqueue(road);
            }
        }

        private void InitTrafficArrays()
        {
            Vector3 newPos = new Vector3(50f, 0f, 0f);
            int maxNumber = AppControl.Global.SavedSettings.TrafficSetup.Count;
            TrafficGameObjects = new GameObject[maxNumber];
            AppControl.Global.TrafficArray = new TrafficLightController[maxNumber];
            for (int i = 0; i < AppControl.Global.SavedSettings.TrafficSetup.Count; i++)
            {
                GameObject tl = InstantiateTrafficLight(AppControl.Global.SavedSettings.TrafficSetup[i], ref newPos);
                
                TrafficLightController tlController = tl.GetComponent<TrafficLightController>();
                tlController.MyIndex = i;
                tlController.HasAmber = AppControl.Global.SavedSettings.TrafficSetup[i].HasAmber;
                tlController.SecondsToGreen = AppControl.Global.SavedSettings.TrafficSetup[i].ToGreenTimer;
                tlController.SecondsToRed = AppControl.Global.SavedSettings.TrafficSetup[i].ToRedTimer;
                if (i == AppControl.Global.SavedSettings.TrafficSetup.Count - 1)
                {
                    tlController.IsLast = true;
                }
                TrafficGameObjects[i] = tl;
                AppControl.Global.TrafficArray[i] = tlController;
            }
            
        }

        private GameObject InstantiateTrafficLight(TrafficLightData trafficLight, ref Vector3 newPos)
        {
            GameObject tl;
            switch (trafficLight.SegmentLength)
            {
                case "S":
                    newPos.z += SmallSegment;
                    tl = Instantiate(SmallTrafficLight, newPos, Quaternion.identity, TrafficLightParent);
                    break;
                case "M":
                    newPos.z += MediumSegment;
                    tl = Instantiate(MediumTrafficLight, newPos, Quaternion.identity, TrafficLightParent);
                    break;
                case "L":
                    newPos.z += LargeSegment;
                    tl = Instantiate(LargeTrafficLight, newPos, Quaternion.identity, TrafficLightParent);
                    break;
                default: // if there were any other character in the array, we set a default instantiation segment type
                    newPos.z += SmallSegment;
                    tl = Instantiate(SmallTrafficLight, newPos, Quaternion.identity, TrafficLightParent);
                    break;
            }
            
            if (newPos.z > terrainLength * TerrainPoolSize + terrainLength)
            {
                tl.SetActive(false);    // Disable in hierarchy if too far from camera
            }
            return tl;
        }

        public static void UsherTerrain(Terrain terrain)
        {
            Vector3 terrainPos = terrain.transform.position;
            float newZPos = terrainLength * TerrainPoolSize + terrain.GetPosition().z;
            float oldZPos = terrainPos.z + terrainLength;
            terrain.transform.position = new Vector3(0.0f, 0.0f, newZPos);
            ERRoad thisRoad = roadPool.Dequeue();
            Vector3[] markers = thisRoad.GetMarkerPositions();
            for (int index = 0; index < markers.Length; index++)
            {
                markers[index].z += (terrainLength * TerrainPoolSize);
            }
            thisRoad.SetMarkerPositions(markers);
            roadPool.Enqueue(thisRoad);

            TrafficLightSwitcher(newZPos, oldZPos);
        }

        private static void TrafficLightSwitcher(float newTerrainZPos, float oldTerrainZPos)
        {
            for (int i = switcherIndex; i < TrafficGameObjects.Length; i++)
            {
                if (TrafficGameObjects[i].transform.position.z <= oldTerrainZPos)
                {
                    Destroy(TrafficGameObjects[i]);
                    switcherIndex++;
                }
                else if (TrafficGameObjects[i].transform.position.z <= newTerrainZPos + terrainLength)
                {
                    TrafficGameObjects[i].SetActive(true);
                }
            }
        }
    } // End class
} // End Namespace
