﻿using UnityEngine;

namespace RoadManagement
{
    [RequireComponent(typeof(BoxCollider))]
    public class OuterCollider : MonoBehaviour
    {
        private TrafficLightController parent;
        private BoxCollider myCollider;
    
        // Start is called before the first frame update
        void Awake()
        {
            myCollider = GetComponent<BoxCollider>();
            parent = GetComponentInParent<TrafficLightController>();
        }
    
        private void OnTriggerEnter(Collider car)
        {
            parent.ReceiveOuterTrigger(myCollider,car);
        }
    }
}
