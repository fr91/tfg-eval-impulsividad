using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace Driving
{
    [RequireComponent(typeof(CarSuspensionSystem), typeof(CarUserController))]
    public class CarPhysicsController : MonoBehaviour
    {
        // Other components in the Vehicle
        private Transform myTransform;
        private BoxCollider carcollider;
        private Rigidbody rigidcar;
        private CarSuspensionSystem suspensionSystem;
        private CarUserController playerController;
        
        public bool DebugDriving = false;
        
        // Speed
        [Range(1, 150)] public float MaxVelocityKmph = 120.0f; // in km/h
        private float maxVelocity;
        private float inverseMaxVelocity;
        internal float velocityMagnitude;
        internal Vector3 localVelocity;
        private float zVel;
        private int zVelSign;
        
        //// Speedometer text
        [SerializeField] private Text velocityText = null;
        //private readonly string velocityUnitText = " km/h"; // readonly is more suitable for a constructor or start/awake method
        private const string VelocityUnitText = " km/h";
        private bool skipTextUpdate = false;

        // Accel Forces
        public float Accel = 8.0f;
        public float TurnAccel = 3.0f;
        public Vector3 Integrator = new Vector3(0.0f, 0.0f, 0.0f);

        // Grip
        [Range(0, 1)] public float GripRatio = 1.0f;
        private Vector3 centerOfTorque;
        private Vector3 centerOfAccel;
        [Range(0.6f, 0.80f)] public float CenterOfTorque = 0.65f; //0.65-0.75f values recommended for easier driving
        [Range(0.5f, 0.80f)] public float CenterOfAccel = 0.65f; //0.65-0.75f values recommended for easier driving

        // Awake is called when the script instance is being loaded
        private void Awake()
        {
            myTransform = GetComponent<Transform>();
            carcollider = GetComponent<BoxCollider>();
            rigidcar = GetComponent<Rigidbody>();
            suspensionSystem = GetComponent<CarSuspensionSystem>();
            playerController = GetComponent<CarUserController>();
            SetCenterOfForces();
        }

        // Start is called before the first frame update
        private void Start()
        {
            maxVelocity = MaxVelocityKmph / 3.6f; // convert to m/s
            inverseMaxVelocity = 1.0f / maxVelocity; // calculate the inverse of the max velocity
            if (velocityText == null)
            {
                skipTextUpdate = true;
            }
            /*
            print("1.- centro de masa: " + rigidcar.centerOfMass);
            print("2.- centro de torque: " + centerOfTorque);
            print("2.- centro de accel: " + centerOfAccel);
            */
        }

        // Update is called once per frame
        private void Update()
        {
#if (UNITY_EDITOR)
            if (DebugDriving)
            {
                Vector3 up = myTransform.up;    // lets cache this to avoid inefficient repeated access of a Unity built-in property
                Debug.DrawRay(transform.TransformPoint(centerOfTorque), -up * suspensionSystem.SuspensionLength, Color.blue);
                Debug.DrawRay(transform.TransformPoint(centerOfAccel), -up * suspensionSystem.SuspensionLength, Color.magenta);
                
            }
#endif
            if (!skipTextUpdate)
            {
                velocityText.text = (velocityMagnitude * 3.6f).ToString("F0") + VelocityUnitText;
            }
            GetNextFrameVelocity();
        }

        // This function is called every fixed framerate frame, if the MonoBehaviour is enabled
        private void FixedUpdate()
        {
            if (suspensionSystem.OffTheGround)
            {
                return;
            }

            if (zVel >= -0.87f && zVel <= 0.87f)
            {
                EnsureStop();
            }

            // Easing for turn input depends on forward velocity
            float easing = EaseSteering();
            ApplyForces(easing);
        }

        private void GetNextFrameVelocity()
        {
            // Update localVelocity field each frame (little performance boost because not doing it on each physic tick)
            localVelocity = myTransform.InverseTransformVector(rigidcar.velocity);
            zVel = localVelocity.z;        // Get forward component from localVelocity vector
            zVelSign = Math.Sign(zVel);    // The function from .NET NOT the one from Unity API
        }

        private float EaseSteering()
        {
            // t = ratio between current velocity and MaxVelicity
            float t = Mathf.Abs(zVel) * inverseMaxVelocity; // t should always be between [0.0-1.0] for the lerp to work out
            float steeringCurve = Mathf.Lerp(0.0f, 1.0f, 2.0f * t * (2.0f - t));
            //print("vel sign: " + zVelSign + " ; zVel: " + zVel + "t: " + t + " -->> Input Turn Curve : " + steeringCurve);
            return steeringCurve;
        }

        private void ApplyForces(float easing)
        {
            Vector3 thrustForce;
            Vector3 projected;
            Vector3 turnForce = new Vector3(playerController.TurnInput * TurnAccel * easing * zVelSign, 0.0f, 0.0f);
            if (playerController.PowerInput < 0.0f)
            {
                thrustForce = CalculateBrakeAccel(playerController.BrakeTarget);
                Vector3 drag = rigidcar.velocity * Mathf.Clamp01(1f - rigidcar.drag * Time.fixedDeltaTime);
                if (zVelSign >= 0.0f)
                {
                    thrustForce -= drag;
                }
                else
                {
                    thrustForce += drag;
                }
                projected = thrustForce - Vector3.Project(thrustForce, suspensionSystem.AvgSurfaceNormal.normalized);
                
                //rigidcar.AddForceAtPosition(myTransform.TransformDirection(projected), myTransform.TransformPoint(centerOfAccel), ForceMode.Acceleration);                
                rigidcar.AddForce(projected, ForceMode.Force);
            }
            if (!CapSpeed())    // if max vel not reached, accel force can be applied:
            {
                thrustForce = new Vector3(0.0f, 0.0f, playerController.PowerInput * Accel);
                projected = thrustForce - Vector3.Project(thrustForce, suspensionSystem.AvgSurfaceNormal.normalized);
                rigidcar.AddForceAtPosition(myTransform.TransformDirection(projected), myTransform.TransformPoint(centerOfAccel));
            }
            rigidcar.AddForceAtPosition(myTransform.TransformDirection(turnForce), myTransform.TransformPoint(centerOfTorque), ForceMode.Force);
            //rigidcar.AddForce(new Vector3(0.0f, -velocityMagnitude * 0.12f, 0.0f),ForceMode.Force);    // Better grip to surface the more velocity you have
            ImproveTurnGrip(GripRatio);
        }

        private void AdjustDrag(float velRatio)
        {
            //linear (0.05, 0.25)
            rigidcar.drag = Mathf.Lerp(0.25f, 0.15f, velRatio);
            //angular (0.25, 1)
            //rigidcar.angularDrag = Mathf.Lerp(1.5f, 0.90f, velRatio);
        }

        private void ImproveTurnGrip(float ratio)
        {
            // get lateral component in local space
            Vector3 velocityComponent = new Vector3(myTransform.InverseTransformDirection(rigidcar.velocity).x, 0.0f, 0.0f);
            rigidcar.AddRelativeForce(-velocityComponent * (ratio + 1.05f), ForceMode.Acceleration);
        }

        // This function caps velocity when no input is given and when max velocity reached
        private bool CapSpeed()
        {
            //velocityMagnitude = rigidcar.velocity.magnitude;
            velocityMagnitude = Vector3.Dot(rigidcar.velocity, myTransform.forward);
            if (velocityMagnitude >= maxVelocity)
            {
                rigidcar.velocity = rigidcar.velocity.normalized * maxVelocity;
                return true;
            }
            return false;
        }

        // Ensures the car stop in the local XZ plane
        private void EnsureStop()
        {
            // First, lets cache some vars:
            Vector3 right = myTransform.right;
            Vector3 forward = myTransform.forward;
            // Because of suspension depends on wheel positions, the car might be slightly tilted.
            // For low velocities, we counter the force from gravity components with this 2 forces:
            Vector3 xStop = -Vector3.Dot(Physics.gravity, right) * right;
            Vector3 zStop = -Vector3.Dot(Physics.gravity, forward) * forward;
            rigidcar.AddForce(zStop, ForceMode.Force);
            rigidcar.AddForce(xStop, ForceMode.Force);
        }

        private void SetCenterOfForces()
        {
            Quaternion storedRotation = rigidcar.rotation;
            rigidcar.rotation = Quaternion.identity;
            Vector3 centerOfMass = rigidcar.centerOfMass;    // lets cache this to avoid inefficient repeated access of built-in property

            //centerOfTorque calculation
            centerOfTorque = new Vector3(0f, 0f, carcollider.bounds.extents.z * CenterOfTorque);
            centerOfTorque = centerOfMass + centerOfTorque;
            centerOfTorque = new Vector3(centerOfTorque.x, centerOfMass.y - 0.2f, centerOfTorque.z);
            centerOfAccel = new Vector3(0f, centerOfMass.y - 0.15f, centerOfMass.z - 1.85f);
            centerOfAccel = new Vector3(0f, centerOfMass.y - 0.15f, centerOfMass.z - 1.85f);
            //centerOfAccel = centerOfMass;
            rigidcar.rotation = storedRotation;
        }
        
        private Vector3 CalculateBrakeAccel(Vector3 targetPos)
        {
            //// a = 0.5 * v0^2 / (x0 - xs)
            //float remaining = (targetPos.z - rigidcar.position.z);
            //float brake = 0.5f * rigidcar.velocity.sqrMagnitude / remaining;
            //return brake;
            ///////////////////////////////
            Vector3 vel = rigidcar.velocity;
            float toVel = 7f;
            //float maxVel = playerController.PeakVelocity
            float maxForce = 155.0f;
            float pGain = 10f;
            float iGain = 0.15f;
            
            Vector3 currentPosition = rigidcar.position + new Vector3(0f, 0f, carcollider.bounds.extents.z);
            Vector3 dist = targetPos - currentPosition;
            // ignore heigh & sideways differences
            dist.x = 0f; 
            dist.y = 0f;
            rigidcar.velocity = new Vector3(0f, rigidcar.velocity.y, rigidcar.velocity.z);
            // calc a target vel proportional to distance (clamped to maxVel)
            Vector3 tgtVel = Vector3.ClampMagnitude(toVel * dist, playerController.PeakVelocity); // PeakVelocity VS rigidcar.velocity
            // calculate the velocity error
            Vector3 error = tgtVel - rigidcar.velocity;
            Integrator += error;
            // calc a force proportional to the error (clamped to brake(maxForce))
            Vector3 force = Vector3.ClampMagnitude(pGain * error + Integrator * iGain, maxForce);
            return force;
        }
    }
}