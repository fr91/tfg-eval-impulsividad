using System;
using System.Diagnostics.CodeAnalysis;
using TFGUtils;
using UnityEngine;

namespace Driving
{
    public enum EngineStatus {Starting, Braking, Stopped, ForceHalt};
    
    [SelectionBase]
    [RequireComponent(typeof(CarSuspensionSystem), typeof(CarPhysicsController))]
    public class CarUserController : MonoBehaviour
    {
        private bool DebugController = false;
        
        // PhysicController related:
        private Vector3 carVelocity;
        private CarPhysicsController physicsController;
        private Rigidbody rigidcar;
        private BoxCollider carcollider;
        
        // Wheels/Suspension Related
        private CarSuspensionSystem suspensionSystem;
        private float[] originalModelRotation;

        // Delegate
        private delegate void DrivingBehaviour();
        private DrivingBehaviour getPlayerInput;

        // Player Input:
        public float PowerInput;
        public float TurnInput;
        private bool accelerating = false;
        
        public delegate void PlayerAction(float boundingBoxFront);
        public static event PlayerAction OnStart;
        public static event PlayerAction OnBrakePush;
        public static event PlayerAction OnStop;

        // Fixed distance braking:
        private float boundingBoxLimit; // The spot from which we'll measure the distance traveled
        private bool justStopped = false;
        private float startBraking;
        private float endBraking;
        internal float PeakVelocity;
        public float BrakeDistance;
        internal Vector3 BrakeTarget;
        
        // IU
        public GameObject FinishUserInterface;
        internal bool finishedTrial = false;

        public enum DrivingType
        {
            NoSteerNoReverse,
            AutoAccel,
            FullManual
        }
        public DrivingType Type;
        


        public static EngineStatus Engine;

        private void Awake()
        {
            rigidcar = GetComponent<Rigidbody>();
            physicsController = GetComponent<CarPhysicsController>();
            suspensionSystem = GetComponent<CarSuspensionSystem>();
            carcollider = GetComponent<BoxCollider>();
            switch (Type)
            {
                // Get player input
                case DrivingType.NoSteerNoReverse:
                    getPlayerInput = InitNoSteerNoReverse;
                    break;
                case DrivingType.AutoAccel:
                    getPlayerInput = InitAutoAccel;
                    break;
                case DrivingType.FullManual:
                default: //case DrivingType.FullManual
                    getPlayerInput = InitFullManual;
                    break;
            }
            SetWheelTurnLimit();
        }

        private void Start()
        {
            boundingBoxLimit = carcollider.bounds.extents.z;
        }

        private void Update()
        {
            carVelocity = physicsController.localVelocity;
            getPlayerInput();
            ModelUpdate();
#if UNITY_EDITOR
            //UnityEditor.SceneView.lastActiveSceneView.FrameSelected();
#endif
        }

        [SuppressMessage("ReSharper", "BitwiseOperatorOnEnumWithoutFlags")]
        [SuppressMessage("ReSharper", "Unity.InefficientPropertyAccess")]
        private void InitNoSteerNoReverse()
        {
            if (accelerating == false && Input.GetAxis("Vertical") > 0.0f)
            {
                PowerInput = 1.0f;
                accelerating = true;
                justStopped = true;
                physicsController.Integrator = Vector3.zero;
                rigidcar.constraints &= ~RigidbodyConstraints.FreezePositionZ;
                startBraking = transform.position.z + boundingBoxLimit;    // So you have a value for when the car is StopImmediately()
                Engine = EngineStatus.Stopped;
                OnStart?.Invoke(startBraking);
            }
            else if (accelerating == true && Input.GetAxis("Vertical") < 0.0f)
            {
                PowerInput = -1.0f;
                startBraking = transform.position.z + boundingBoxLimit;
                PeakVelocity = rigidcar.velocity.z;
                BrakeTarget = new Vector3(0f, 0f, startBraking + BrakeDistance);
                accelerating = false;
                Engine = EngineStatus.Braking;
                OnBrakePush?.Invoke(startBraking);
            }
            else if (accelerating == false && (ProjectUtils.StopOrReverse(carVelocity.z) || Engine == EngineStatus.ForceHalt))
            {
                PowerInput = 0.0f;
                if (justStopped)
                {
                    justStopped = false;
                    endBraking = transform.position.z + boundingBoxLimit;
                    if (Engine != EngineStatus.ForceHalt)
                        Engine = EngineStatus.Stopped;
                    OnStop?.Invoke(endBraking);
#if (UNITY_EDITOR)
                    if (DebugController)
                    {
                        //print(startBraking + " ||| " + endBraking);
                        print("Frenada en : " + (endBraking - startBraking) + " metros");
                    }
#endif
                }
                rigidcar.constraints |= RigidbodyConstraints.FreezePositionZ;
            }

            TurnInput = 0.0f;
        }

        public void StopImmediately()
        {
            accelerating = false;
            rigidcar.constraints |= RigidbodyConstraints.FreezePositionZ;
            Engine = EngineStatus.ForceHalt;
        }
        
        public void DrivingTrialEnd()
        {
            PowerInput = 0f;
            TurnInput = 0f;
            rigidcar.constraints = RigidbodyConstraints.FreezeAll;
            finishedTrial = true;
            // Show UI for finish
            FinishUserInterface.SetActive(true);
        }

        private void InitAutoAccel()
        {
            Engine = EngineStatus.Starting;
            PowerInput = 0.9f;
            TurnInput = Input.GetAxis("Horizontal");
        }

        private void InitFullManual()
        {
            PowerInput = Input.GetAxis("Vertical");
            TurnInput = Input.GetAxis("Horizontal");
        }

        private void SetWheelTurnLimit()
        {
            originalModelRotation = new float[suspensionSystem.WheelObjects.Length];
            originalModelRotation[0] = suspensionSystem.WheelObjects[0].transform.localEulerAngles.y;
            originalModelRotation[1] = suspensionSystem.WheelObjects[1].transform.localEulerAngles.y;
        }
        
        private void ModelUpdate()
        {
            for (int i = 0; i < suspensionSystem.WheelObjects.Length-2; i++)
            {
                Vector3 wheelRotation = suspensionSystem.WheelObjects[i].transform.localEulerAngles;
                float targetAngleY = Mathf.Lerp(0f, 25f, Mathf.Abs(TurnInput));
                targetAngleY *= Mathf.Sign(TurnInput);
                targetAngleY += originalModelRotation[i];
                suspensionSystem.WheelObjects[i].transform.localEulerAngles = new Vector3(wheelRotation.x, targetAngleY, wheelRotation.z);
            }
        }

        private void OnDestroy()
        {
            Delegate.RemoveAll(getPlayerInput, getPlayerInput);
        }
    }
}