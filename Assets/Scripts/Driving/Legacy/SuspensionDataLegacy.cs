using UnityEngine;

namespace Driving.Legacy
{
    [System.Serializable]
    public class SuspensionDataLegacy {

        private Rigidbody rigidcar;
        private Transform transform;
        public bool hitCheck;
        public RaycastHit hitAttr;
        private Vector3 localSpaceOrigin;
        public Vector3 Origin
        {
            get
            {
                return transform.TransformPoint(localSpaceOrigin);
            }
            set
            {
                localSpaceOrigin = value;
            }
        }
        public float Stiffness;
        public float SuspensionLength;
        public float CompresionRatio
        {
            get
            {
                return hitCheck ? (SuspensionLength - hitAttr.distance) / SuspensionLength : 0.0f;
            }
        }

        //
        // Summary:
        //     Constructor for SuspensionDataLegacy Class.
        //
        // Parameters:
        //   vehicleTransform:
        //     The transform attached to the vehicle you're creating SuspensionDataLegacy for
        public SuspensionDataLegacy(Transform vehicleTransform, Rigidbody vehicleRigidbody, in Vector3 originOfRaycast, float suspensionLength)
        {
            hitCheck = false;
            transform = vehicleTransform;
            rigidcar = vehicleRigidbody;
            Origin = originOfRaycast;
            SuspensionLength = suspensionLength;
            Stiffness = 5.0f;
        }

        //
        // Summary:
        //     Constructor for SuspensionDataLegacy Class.
        //
        // Parameters:
        //   vehicleTransform:
        //     The transform attached to the vehicle you're creating a suspension system
        //
        //   stiffness:
        //     How rigid the suspension feels: 
        //      · Higher values means more elevated from ground but more sensitive & bouncier suspension
        //      · Lower values means more sagged down vehicle but will absorb more impact and it'll more smooth to ride 
        public SuspensionDataLegacy(Transform vehicleTransform, Rigidbody vehicleRigidbody, in Vector3 originOfRaycast, float suspensionLength, float stiffness)
        {
            hitCheck = false;
            transform = vehicleTransform;
            rigidcar = vehicleRigidbody;
            Origin = originOfRaycast;
            Stiffness = stiffness;
            SuspensionLength = suspensionLength;

        }
    
        public bool CalculateSuspension()
        {
            /*
         * transform.up VS Vector3.up (in the context of the next calculations):
         * ==============================================================================
         * 
         *  · Vector3.up: is always a normal vector to the X plane
         * 
         *  · transform.up: direction depends on the rotation of this gameObject, it would always be a normal vector to the *LOCAL* x-axis of the object
         * 
         * If you want the suspensions to work correctly on surfaces not parallel with global x-axis (tilted roads etc) you should use transform.up
         * 
        */
            Vector3 currentSuspension, newSuspension, deltaSuspension;

            if (hitCheck = Physics.Raycast(Origin, -transform.up, out hitAttr, SuspensionLength))
            {
                //cr = (suspensionLength - hitAttr.distance) / suspensionLength;
                currentSuspension = Vector3.Project(rigidcar.GetPointVelocity(Origin), transform.up);
                newSuspension = transform.up * Stiffness * CompresionRatio;
                deltaSuspension = newSuspension - currentSuspension;
                rigidcar.AddForceAtPosition(deltaSuspension, Origin, ForceMode.Acceleration);
            }
            else
            {
                hitAttr.normal = transform.up;
                hitAttr.point = Origin - transform.up * SuspensionLength;
                hitAttr.distance = SuspensionLength;
            }

            return hitCheck;
        }


        public void DrawDebugRays()
        {
            if (hitCheck)
            {
                Debug.DrawRay(Origin, transform.up * -hitAttr.distance, new Color32(0, 255, 0, 255), 0.0f, false);
            }
            else
            {
                Debug.DrawRay(Origin, -transform.up * SuspensionLength, new Color32(255, 128, 16, 255), 0.0f, false);
            }
        }

        public void DrawDebugLabels()
        {
            Vector3 screenPosition;
            if (hitCheck)
            {
                screenPosition = Camera.main.WorldToScreenPoint(hitAttr.point);
            }
            else
            {
                screenPosition = Camera.main.WorldToScreenPoint(Origin - transform.up * SuspensionLength);
            }

            //GUI.Label(new Rect(screenPosition.x, Screen.height - screenPosition.y, 25f, 20f), cr.ToString());
            Rect rect = new Rect(screenPosition.x, Screen.height - screenPosition.y, 25f, 20f);
            GUI.Label(rect, (CompresionRatio).ToString());
        }
    }
}
