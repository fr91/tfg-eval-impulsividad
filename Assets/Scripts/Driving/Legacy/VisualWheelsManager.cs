using UnityEngine;

namespace Driving.Legacy
{
    public class VisualWheelsManager : MonoBehaviour
    {
        // Visual Wheels
        public GameObject[] WheelObjects; // = new GameObject[4];
        private MeshFilter[] WheelMeshes; // = new MeshFilter[4];
        private Vector3[] suspensionVel; // = new Vector3[4];

        // Awake is called when the script instance is being loaded
        private void Awake()
        {
            InitializeGraphicWheels();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void InitializeGraphicWheels()
        {
            WheelMeshes = new MeshFilter[WheelObjects.Length];
            suspensionVel = new Vector3[WheelObjects.Length];
            for (int i = 0; i < WheelMeshes.Length; i++)
            {
                WheelMeshes[i] = WheelObjects[i].GetComponent<MeshFilter>();
            }
        }


        public void ModelSuspension(int wheelIndex, SuspensionDataLegacy WheelSuspension)
        {
            //WheelObjects[wheelIndex].transform.position = WheelSuspension[wheelIndex].hitAttr.point + Vector3.Scale(Vector3.up, WheelMeshes[wheelIndex].sharedMesh.bounds.extents);
            Vector3 target = WheelSuspension.hitAttr.point + Vector3.Scale(Vector3.up, WheelMeshes[wheelIndex].sharedMesh.bounds.extents);
            WheelObjects[wheelIndex].transform.position = Vector3.SmoothDamp(WheelObjects[wheelIndex].transform.position, target, ref suspensionVel[wheelIndex], 0.15f/*, Mathf.Infinity, Time.fixedDeltaTime*/);
            //WheelObjects[wheelIndex].transform.rotation.eulerAngles = Mathf.SmoothDampAngle(WheelObjects[wheelIndex].transform.rotation.eulerAngles, )
        }
    }
}
