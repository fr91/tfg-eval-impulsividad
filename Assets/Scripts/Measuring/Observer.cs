﻿using System;
using System.Collections.Generic;
using App;
using Driving;
using RoadManagement;
using TFGUtils;
using UnityEditor;
using UnityEngine;

namespace Measuring
{
    public class Observer : MonoBehaviour
    {
        private TrafficLightController[] trafficControllers;            // A shorcut name for "AppControl.Global.TrafficArray"
        public MeasureData[] Measurings;  // Array of measures
        private int current = 0;
        private float[] engineStartTimestamp;
        private int aux;
        private bool done;
        public GameObject FinishUserInterface;
        
        private void Start()
        {
            Measurings = new MeasureData[AppControl.Global.SavedSettings.TrafficSetup.Count];
            engineStartTimestamp = new float[Measurings.Length];
            for (int i = 0; i < Measurings.Length; i++)
            {
                engineStartTimestamp[i] = -1f;
                Measurings[i] = new MeasureData();
            }
            trafficControllers = AppControl.Global.TrafficArray;
            trafficControllers[current].OnCompletion += OnSegmentCompletion;
            trafficControllers[current].OnTurnRed += OnRed;
        }

        private void OnEnable()
        {
            CarUserController.OnStart += OnCarStart;
            CarUserController.OnBrakePush += OnCarBraking;
            CarUserController.OnStop += OnCarStopped;
        }

        private void OnDisable()
        {
            CarUserController.OnStart -= OnCarStart;
            CarUserController.OnBrakePush -= OnCarBraking;
            CarUserController.OnStop -= OnCarStopped;
        }
        
        private void OnRed(int nextIndex)
        {
            aux = nextIndex;
            done = false;
        }
        

        private void Update()
        {
            if (!done)
            {
                if (trafficControllers[aux].GreenTimestamp > -1f && engineStartTimestamp[aux] > -1f)
                {
                    Measurings[aux].ElapsedSinceGreen = engineStartTimestamp[aux] - trafficControllers[aux].GreenTimestamp;
                    done = true;
                }
            }
        }
        
        private void OnCarStart(float startSpot)
        {
            if (!trafficControllers[current].IsLast && trafficControllers[current].ReadyToPlayerTimestamp)
            {
                if (engineStartTimestamp[aux] < 0f)
                {
                    engineStartTimestamp[aux] = Time.time;
                    Measurings[aux].StartedOnGreen = trafficControllers[aux].TrafficState == LightsColor.Green;
                }
            }
            else if (trafficControllers[current].IsLast && trafficControllers[current].ReadyToPlayerTimestamp && current == 0)
            {
                if (engineStartTimestamp[current] < 0f)
                {
                    engineStartTimestamp[current] = Time.time;
                    Measurings[current].StartedOnGreen = trafficControllers[current].TrafficState == LightsColor.Green;
                }
            }
        }

        private void OnCarBraking(float startToBrakeSpot)
        {
            if (trafficControllers[current].TrafficState != LightsColor.Red)
            {
                Measurings[current].CommissionTimes++;
                AddCommissionSpot(startToBrakeSpot);
            }
        }

        private void OnCarStopped(float stoppedSpot)
        {
            if (trafficControllers[current].TrafficState == LightsColor.Red)
            {
                Measurings[current].MetersToStopLine = GetDistanceToStopLine(stoppedSpot);
                Measurings[current].StopOverflow = HasOverflowed();
            }

            if (trafficControllers[current].IsLast)
            {
                //print("FIN DE LA PRUEBA");
                TrialEnd();
            }
        }

        // This is directly called when trafficControllers[current].completed == true
        private void OnSegmentCompletion()
        {
            trafficControllers[current].OnTurnRed -= OnRed;
            trafficControllers[current].OnCompletion -= OnSegmentCompletion;
            current++;
            if (current < trafficControllers.Length)
            {
                trafficControllers[current].OnCompletion += OnSegmentCompletion;
                trafficControllers[current].OnTurnRed += OnRed;
            }
        }

        private float GetDistanceToStopLine(float stoppedSpot)
        {
            return trafficControllers[current].GetStopColliderPosition() - stoppedSpot;
        }

        private bool HasOverflowed()
        {
            return (CarUserController.Engine == EngineStatus.ForceHalt);
        }

        private void AddCommissionSpot(float startToBrakeSpot)
        {
            if (Measurings[current].CommissionSpots == null)
            {
                Measurings[current].CommissionSpots = new List<float>();
            }
            Measurings[current].CommissionSpots.Add(startToBrakeSpot);
        }

        private void TrialEnd()
        {
            // Save Results
            StartStopTrialData results = new StartStopTrialData {StartStopResults = Measurings};
            JsonHelper.SaveStartStopResults(results, AppControl.Global.DataPath);
            
            // Deactivate user input
            GameObject player = GameObject.FindWithTag("Player");
            player.GetComponent<CarUserController>().enabled = false;
            
            // Show UI for finish
            FinishUserInterface.SetActive(true);
            
        }
    }
}
