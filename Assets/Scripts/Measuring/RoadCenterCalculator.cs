﻿using System;
using System.Collections;
using System.IO;
using Driving;
using EasyRoads3Dv3;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using static Driving.CarUserController;

namespace Measuring
{
     
    public class RoadCenterCalculator : MonoBehaviour
    {
        //Car references
        public GameObject Car;
        public Transform CenterRef;
        private Rigidbody rigidcar;
        private CarPhysicsController physicsController;
        private CarSuspensionSystem suspensionController;
        private CarUserController userController;
        public Renderer BodyWorkRenderer;

        // Center of the road
        public int CurrentElement = 0;
        public float RefVelocity;
        public float Distance;
        public Vector3 TargetRot;

        // Road references
        private ERRoad myRoad;
        
        // Saving results
        private static string finalFileName;

        // Use this for initialization
        void Start()
        {
            string currentSceneName = SceneManager.GetActiveScene().name;
            string fileName = DateTime.Now.ToString("yyyy.MM.dd HH.mm.ss");
            finalFileName = fileName + " - " + currentSceneName + ".txt";
            WriteString("Time : Distance to center");
            
            physicsController = Car.GetComponent<CarPhysicsController>();
            suspensionController = Car.GetComponent<CarSuspensionSystem>();
            userController = Car.GetComponent<CarUserController>();
            rigidcar = Car.GetComponent<Rigidbody>();
            
            ERRoadNetwork myRoadNetwork = new ERRoadNetwork();
            myRoad = myRoadNetwork.GetRoadByName("Plain Sand Path 001");
            Car.transform.position = myRoad.GetMarkerPosition(0);
        }

        static void WriteString(string feed)
        {
            string path = Path.Combine(Application.dataPath, finalFileName);
            
            // Writing some text to the txt file:
            StreamWriter writer = new System.IO.StreamWriter(path, true);
            writer.WriteLine(feed);
            writer.Close();
            
        }

        private void FixedUpdate()
        {
            //RefVelocity = physicsController.velocityMagnitude; //physicsController.localVelocity.z;
            RefVelocity = Vector3.Dot(rigidcar.velocity, transform.forward);
            //print("dot product: " + RefVelocity);
            if (RefVelocity < -0.25f)
            {
                ResetToRoadCenter();
            }
            float forwardDiff = transform.localPosition.z - CenterRef.localPosition.z;
            if (forwardDiff > 1f)
            {
                Distance += 0.5f * RefVelocity * Time.fixedDeltaTime;
            }
            else if (forwardDiff < 1f)
            {
                Distance += 1.5f * RefVelocity * Time.fixedDeltaTime;
            }
            else
            {
                Distance +=  RefVelocity * Time.fixedDeltaTime;
            }

            if (!userController.finishedTrial)
            {
                WriteString(Time.fixedTime + ": " + (-1f * transform.localPosition.x));
            }
        }

        // Update is called once per frame
        void Update()
        {
            
            Vector3 roadCenter = myRoad.GetPosition(Distance , ref CurrentElement);
            roadCenter.y = 0.25f;
            transform.position = (roadCenter);
            TargetRot = myRoad.GetLookatSmooth(Distance, CurrentElement);
            transform.rotation = Quaternion.LookRotation(TargetRot, Vector3.up);
            //print(myRoad.GetLength());
        }
        
        public void ResetToRoadCenter()
        {
            rigidcar.velocity = Vector3.zero;
            rigidcar.angularVelocity = Vector3.zero;
            rigidcar.isKinematic = true;
            
            rigidcar.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            
            
            physicsController.enabled = false;
            suspensionController.enabled = false;
            userController.enabled = false;
            
            rigidcar.position = new Vector3(transform.position.x, 0.15f, transform.position.z);   //the position of the game object with this script attached
            rigidcar.rotation = Quaternion.LookRotation(TargetRot, Vector3.up);
            
            StartCoroutine(ReenableCar());
        }


        IEnumerator ReenableCar()
        {
            for (int i = 0; i < 6; i++)
            {
                Color32 originalColor = new Color32(221, 30, 32, 255); 
                BodyWorkRenderer.material.color = new Color32(60, 0, 5, 255);
                yield return new WaitForSeconds(.1f);
                BodyWorkRenderer.material.color = originalColor;
                yield return new WaitForSeconds(.1f);
            }
            //WriteString("*** " +Time.fixedTime + ": " + ("RESET al centro de la carretera ***"));
            //yield return new WaitForSeconds(1f);
            physicsController.enabled = true;
            suspensionController.enabled = true;
            userController.enabled = true;
            rigidcar.isKinematic = false;
            rigidcar.constraints = RigidbodyConstraints.None;
        }


    }
    
    
}
