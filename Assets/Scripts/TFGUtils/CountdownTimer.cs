using System;
using System.Diagnostics.CodeAnalysis;
using App;

namespace TFGUtils
{
    public class CountdownTimer
    {
        public bool Enabled;
        public float Interval;
        private float countdown;

        public delegate void CountdownEvent();
        public event CountdownEvent OnTimeUp;

        public CountdownTimer(float seconds, CountdownEvent handler)
        {
            Enabled = false;
            Interval = seconds;
            OnTimeUp += handler;
        }

        public CountdownTimer(CountdownEvent handler)
        {
            Enabled = false;
            Interval = 0f;
            OnTimeUp += handler;
        }

        public void StartTimer()
        {
            countdown = Interval;
            Enabled = true;
        }

        public void ForceUnbind()
        {
            Enabled = false;
            OnTimeUp -= OnTimeUp;
        }

        /// <summary>
        /// Checks every Update() or FixedUpdate() if the countdown has ended
        /// </summary>
        /// <param name="unityTimeElapsed">Will be Time.deltaTime or Time.fixedDeltaTime depending if used in Update() or FixedUpdate()</param>
        public void TickCheck(float unityTimeElapsed)
        {
            if (!Enabled) return;
            countdown -= unityTimeElapsed;
            if (countdown <= 0f)
            {
                OnTimeUp?.Invoke();
                Enabled = false;
#if (UNITY_EDITOR)
                if (AppControl.Global.Debuging)
                {
                    UnityEngine.MonoBehaviour.print("Timer countdown was: " + OnTimeUp?.Method.Name + ": " + (countdown - Interval).ToString("F3"));
                }
#endif
                OnTimeUp -= OnTimeUp;        //Auto-dispose of the timer references when timer finishes 
            }
        }

        public void UnbindTimerHandler(CountdownEvent handler)
        {
            OnTimeUp -= handler;
        }
    }
}