#if (UNITY_EDITOR)

using UnityEngine;

namespace TFGUtils
{
    public class KeepSceneTabActive : MonoBehaviour
    {

        public bool KeepSceneViewActive;

        void Start()
        {
            if (this.KeepSceneViewActive && Application.isEditor)
            {
                UnityEditor.SceneView.FocusWindowIfItsOpen(typeof(UnityEditor.SceneView));
            }
        }
    }
}

#endif
