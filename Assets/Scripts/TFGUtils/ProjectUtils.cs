using System;
using System.Runtime.InteropServices;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TFGUtils
{
    public static class ProjectUtils
    {
        /// <summary>
        /// Returns -1 or 1 with equal change.
        /// </summary>
        public static int RandomSign
        {
            get { return (Random.value < 0.5f) ? -1 : 1; }
        }

        /// <summary>
        /// Returns true if velocity is negative or near zero.
        /// Returns false if not.
        /// </summary>
        public static bool StopOrReverse(float velocity)
        {
            if (Math.Abs(velocity) < 0.17f || velocity < 0.0f)    // velocity < 0, OR velocity == 0, with a tolerance of 0.05
            {
                return true;
            }
            return false;        // (velocity >> 0.0f) 
        }
        
        public static bool EqualsAny(this string haystack, params string[] needles)
        {
            foreach (string needle in needles)
            {
                if (haystack.Equals(needle, StringComparison.OrdinalIgnoreCase))
                    return true;
            }
            return false;
        }
    }
}
